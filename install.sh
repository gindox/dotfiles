#!/bin/bash

install_home=~/
current_cwd="$(pwd)"

sh nano/nano.sh
sh osx/osx.sh

if [ -f "$install_home"/.nanorc ]; then
	while true; do
		read -p ".nanorc exists, overwrite? (y/n)" choice
		case "$choice" in
			y|Y ) cp ./nanorc ~/.nanorc; break;;
			n|N ) exit;;
			* ) echo "(y/n)";;
		esac
	done
else
	cp ./nanorc ~/.nanorc;
fi

sh saschet/install.sh
