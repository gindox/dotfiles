#!/bin/bash

install_home=~/
current_cwd="$(pwd)"

# Avoid creating .DS_Store files on network volumes
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true


if [ -d "/Applications/Sublime Text 2.app" ] && [ ! -f /opt/local/bin/subl ]; then
	sudo ln -s "/Applications/Sublime Text 2.app/Contents/SharedSupport/bin/subl" /opt/local/bin/subl
fi

if [ -f "$install_home"/.profile ]; then
	while true; do
		read -p ".profile additions will be added (y/n)" choice
		case "$choice" in
			y|Y ) echo "$current_cwd"/profile >> "$install_home"/.profile; break;;
			n|N ) exit;;
			* ) echo "(y/n)";;
		esac
	done
else
	echo "$current_cwd"/profile >> "$install_home"/.profile;
fi